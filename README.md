# README #
This is an Android app which connects to the Discogs API and fetches release information for an artist. It uses oAuth 1.0a authentication as is required by Discogs (in order to request images). It shows information in a ListView, fed by a CursorAdapter and also a detailed view. It persists data in the phone's database for offline viewing.

### How do I get set up? ###

* Summary of set up
- You will need a Discogs account if you want to attempt to load images. Chances are however that even then Discogs will respond with HTTP-404 not found to your requests :/ . No harm trying though!

* Dependencies
- All dependencies are included as jars in the project. You don't need anything more.

### Who do I talk to? ###

* Emmanouil (Manolis) Spanoudakis