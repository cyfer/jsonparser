package uk.co.cyferapps.jsonparser.util;

public class Constants {
	
	public static final String TAG = "JSONParser";
	public static final String EXTRA_OAUTH_TOKEN_RECEIVED = "extra_oauth_token_received";
	public static final String EXTRA_TWO_PANE = "extra_two_pane";
	public static final String RELEASE_ID = "release_id";

}
