package uk.co.cyferapps.jsonparser.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class Preferences {

	public static String PREF_JSONPARSER_AUTH_TOKEN = "pref_jsonparser_auth_token";
	public static String PREF_JSONPARSER_AUTH_TOKEN_SECRET = "pref_jsonparser_auth_token_secret";
	
	
	
	public static String getStringPreference(Context ctx, String prefName, String defValue){
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
		return prefs.getString(prefName, defValue);
		
	}
	
	public static void setStringPreference(Context ctx, String prefName, String value){
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ctx);
		Editor ed = prefs.edit();
		ed.putString(prefName, value);
		ed.commit();
	}
}
