package uk.co.cyferapps.jsonparser.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.telephony.TelephonyManager;

public class NetworkUtil {
	
	
	public static boolean isInternetAvailable(Context context){
		return isWiFiNetworkAvailable(context) || isDataNetworkAvailable(context);
	}

	private static boolean isDataNetworkAvailable(Context context) {
		ConnectivityManager conManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = conManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		return (null != info && info.isConnectedOrConnecting());
	}

	private static boolean isWiFiNetworkAvailable(Context context) {
		ConnectivityManager conManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = conManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		return (null != info && info.isConnectedOrConnecting());
	}
}
