package uk.co.cyferapps.jsonparser.util;

import java.io.ByteArrayInputStream;

import android.graphics.drawable.Drawable;

public class ConvertUtil {

	public static Drawable getDrawableFromBytes(byte[] imageBytes) {

		if (imageBytes != null) {
			ByteArrayInputStream is = new ByteArrayInputStream(imageBytes);
			return Drawable.createFromStream(is, "articleImage");
		} else
			return null;
	}

}
