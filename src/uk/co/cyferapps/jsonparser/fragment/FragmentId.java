package uk.co.cyferapps.jsonparser.fragment;

public enum FragmentId {
	FRAGMENT_RELEASE_LIST, FRAGMENT_RELEASE_DETAILS

}
