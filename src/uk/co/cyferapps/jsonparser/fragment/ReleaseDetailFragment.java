package uk.co.cyferapps.jsonparser.fragment;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uk.co.cyferapps.jsonparser.JSONParserApplication;
import uk.co.cyferapps.jsonparser.R;
import uk.co.cyferapps.jsonparser.database.DataManager;
import uk.co.cyferapps.jsonparser.discogs.api.DiscogsAPI;
import uk.co.cyferapps.jsonparser.discogs.api.ImageResponse;
import uk.co.cyferapps.jsonparser.discogs.api.ReleaseDetailsResponse;
import uk.co.cyferapps.jsonparser.domain.Image;
import uk.co.cyferapps.jsonparser.util.Constants;
import uk.co.cyferapps.jsonparser.util.ConvertUtil;
import uk.co.cyferapps.jsonparser.util.NetworkUtil;
import uk.co.cyferapps.jsonparser.util.Preferences;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ReleaseDetailFragment extends Fragment {
	public static final String ARG_ITEM_ID = "item_id";
	private Activity mActivity;
	private DiscogsAPI mAPI;
	private JSONParserApplication mApplication;
	private DataManager mDataManager;
	private long mReleaseId;
	private TextView mTitleView;
	private TextView mYearView;
	private TextView mCountryView;
	private TextView mUriView;
	private TextView mNotesView;
	private TextView mImageView;
	private LinearLayout mImagesLayout;
	
	public ReleaseDetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments().containsKey(ARG_ITEM_ID)) {
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.release_details,
				container, false);
		return rootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstance) {
		super.onActivityCreated(savedInstance);
		mActivity = getActivity();
		mApplication = (JSONParserApplication) mActivity.getApplication();
		mDataManager = mApplication.getDataManager();
		mAPI = mApplication.getDiscogsClient().api;
		mReleaseId = getArguments().getLong(Constants.RELEASE_ID);
		setHasOptionsMenu(true);
		
		mTitleView = (TextView) mActivity.findViewById(R.id.details_title);
		mYearView = (TextView) mActivity.findViewById(R.id.details_year);
		mCountryView = (TextView) mActivity.findViewById(R.id.details_country);
		mUriView = (TextView) mActivity.findViewById(R.id.details_url);
		mUriView.setMovementMethod(LinkMovementMethod.getInstance());
		mNotesView = (TextView) mActivity.findViewById(R.id.details_notes);
		mImageView = (TextView) mActivity.findViewById(R.id.details_images);
		mImagesLayout = (LinearLayout) mActivity.findViewById(R.id.images_layout);
		
		if(mReleaseId != 0)
		getRelease();
		else{
			mTitleView.setText(getString(R.string.no_details_available));
			mYearView.setVisibility(View.GONE);
			mCountryView.setVisibility(View.GONE);
			mUriView.setVisibility(View.GONE);
			mNotesView.setVisibility(View.GONE);
			mImageView.setVisibility(View.GONE);
		}
		
	}
	
	private void getRelease() {
		if (NetworkUtil.isInternetAvailable(mActivity))
			updateReleaseDetails();
		else {
			if(isAdded())
			Toast.makeText(mActivity, getString(R.string.no_connection),
					Toast.LENGTH_LONG).show();
			
			restoreFromDB();
		}
	}
	
	private void restoreFromDB() {
		//if release details is already in the db, show saved data
		if(isAdded())
		Toast.makeText(mActivity, getString(R.string.showing_last_results),
				Toast.LENGTH_LONG).show();
		
		updateLayouts();
	}
	
	private void updateReleaseDetails() {
		mAPI.getReleaseDetails(String.valueOf(mReleaseId), new Callback<ReleaseDetailsResponse>() {

			@Override
			public void failure(RetrofitError error) {
				
			}

			@Override
			public void success(ReleaseDetailsResponse releaseDetailsResponse, Response response) {
				Log.d(Constants.TAG,
						"Success getting release details. ReleaseDetailsResponse: "
								+ releaseDetailsResponse.toString());
				saveReleaseDetailsInDB(releaseDetailsResponse);
			}

			private void saveReleaseDetailsInDB(
					ReleaseDetailsResponse releaseDetailsResponse) {
				new AsyncTask<ReleaseDetailsResponse, Void, String>() {

					@Override
					protected String doInBackground(
							ReleaseDetailsResponse... params) {
						mDataManager.deleteReleaseDetails(params[0].getId());
						mDataManager.saveReleaseDetails(params[0]);
						return null;
					}

					@Override
					protected void onPostExecute(String result) {
						if(isAdded())
						Toast.makeText(mActivity,
								getString(R.string.data_saved),
								Toast.LENGTH_LONG).show();
						
							updateLayouts();
						if (Preferences.getStringPreference(mActivity,
								Preferences.PREF_JSONPARSER_AUTH_TOKEN,
								null) != null) {
							getImages();
						}
					}
				}.execute(releaseDetailsResponse);
			}
		});
		
	}
	
	private void getImages() {
		List<Image> images = mDataManager.getImagesForRelease(mReleaseId);
		String filename;
		mImagesLayout.removeAllViewsInLayout();
		for (Image image : images) {
			final String fileUri = image.getUri();
			Log.d(Constants.TAG, "Attempting to get image: " + fileUri);
			if (image.getUri().contains("image/")){
				filename = image.getUri().split("image/")[1];
			}
			else
				continue;

			Log.d(Constants.TAG, "Attempting to get image filename: " + filename);
			mAPI.getImageFromURL(filename, new Callback<ImageResponse>() {

				@Override
				public void failure(RetrofitError error) {
					Log.d(Constants.TAG,
							"Failed to get image: " + error.getMessage());
					LayoutParams params = new LayoutParams(
			                LayoutParams.WRAP_CONTENT,
			                LayoutParams.WRAP_CONTENT);
					ImageView imageView = new ImageView(mActivity);
					imageView.setLayoutParams(params);
					imageView.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_action_about));
					mImagesLayout.addView(imageView);
				}

				@Override
				public void success(ImageResponse imageResponse,
						Response response) {
					Log.d(Constants.TAG,
							"Successfully got image! ImageResponse:  "
									+ imageResponse.toString());
					mDataManager.updateReleaseImage(mReleaseId, fileUri, imageResponse.getImage());
					
					LayoutParams params = new LayoutParams(
			                LayoutParams.WRAP_CONTENT,
			                LayoutParams.WRAP_CONTENT);
					ImageView imageView = new ImageView(mActivity);
					imageView.setLayoutParams(params);
					imageView.setImageDrawable(ConvertUtil.getDrawableFromBytes(imageResponse.getImage()));
					mImagesLayout.addView(imageView);
				}
			});
		}
	}
	
	private void updateLayouts(){
		ReleaseDetailsResponse releaseDetails = mDataManager.getReleaseDetails(mReleaseId);
		if (releaseDetails != null){
		mTitleView.setText(releaseDetails.getTitle());
		mYearView.setText(releaseDetails.getYear());
		if(isAdded()){
		mCountryView.setText(String.format(getString(R.string.country), releaseDetails.getCountry()));
		mUriView.setText(String.format(getString(R.string.url), releaseDetails.getUri()));
		mNotesView.setText(String.format(getString(R.string.notes), releaseDetails.getNotes()));
		}
		}
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_about:
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					mActivity);

			alertDialogBuilder.setTitle(getString(R.string.about_jsonparser));
			alertDialogBuilder
					.setMessage(getString(R.string.about_jsonparser_developer)
							+ "\n" + getString(R.string.version) + "\n"
							+ getString(R.string.about_jsonparser_support));
			alertDialogBuilder.setCancelable(true);
			alertDialogBuilder.setPositiveButton(
					getString(R.string.email_support),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							Intent it = new Intent(Intent.ACTION_SEND);
							it.putExtra(
									Intent.EXTRA_EMAIL,
									new String[] { getString(R.string.support_email) });
							it.putExtra(Intent.EXTRA_SUBJECT,
									getString(R.string.app_name));
							it.putExtra(Intent.EXTRA_TEXT,
									getString(R.string.app_name) + ", "
											+ getString(R.string.version));
							it.setType("text/plain");
							startActivity(Intent.createChooser(it,
									"Choose Email Client"));
						}

					});
			alertDialogBuilder.setNegativeButton(android.R.string.ok,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.cancel();
						}
					});
			alertDialogBuilder.create().show();

			break;
		case R.id.action_refresh:
			getRelease();
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
