package uk.co.cyferapps.jsonparser.fragment;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uk.co.cyferapps.jsonparser.JSONParserApplication;
import uk.co.cyferapps.jsonparser.R;
import uk.co.cyferapps.jsonparser.database.DataManager;
import uk.co.cyferapps.jsonparser.database.ReleaseCursorAdapter;
import uk.co.cyferapps.jsonparser.discogs.api.DiscogsAPI;
import uk.co.cyferapps.jsonparser.discogs.api.ImageResponse;
import uk.co.cyferapps.jsonparser.discogs.api.ReleaseResponse;
import uk.co.cyferapps.jsonparser.domain.Release;
import uk.co.cyferapps.jsonparser.util.Constants;
import uk.co.cyferapps.jsonparser.util.NetworkUtil;
import uk.co.cyferapps.jsonparser.util.Preferences;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ReleaseListFragment extends ListFragment {

	private static final String STATE_ACTIVATED_POSITION = "activated_position";
	private Callbacks mCallbacks = sDummyCallbacks;
	private int mActivatedPosition = ListView.INVALID_POSITION;
	private Activity mActivity;
	private DiscogsAPI mAPI;
	private JSONParserApplication mApplication;
	private DataManager mDataManager;
	private ReleaseCursorAdapter mAdapter;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public ReleaseListFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup rootView = (ViewGroup) inflater.inflate(
				R.layout.release_list, container, false);
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		// Restore the previously serialized activated item position.
		if (savedInstanceState != null
				&& savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
			setActivatedPosition(savedInstanceState
					.getInt(STATE_ACTIVATED_POSITION));
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstance) {
		super.onActivityCreated(savedInstance);

		mActivity = getActivity();
		mApplication = (JSONParserApplication) mActivity.getApplication();
		mDataManager = mApplication.getDataManager();
		mAPI = mApplication.getDiscogsClient().api;
		mAdapter = new ReleaseCursorAdapter(mActivity,
				mDataManager.getReleasesCursor());
		setListAdapter(mAdapter);
		setHasOptionsMenu(true);

		if (null != getArguments()
				&& getArguments().containsKey(Constants.EXTRA_TWO_PANE)){
			setActivateOnItemClick(getArguments().getBoolean(
					Constants.EXTRA_TWO_PANE));
		}
			
		if (null != getArguments()
				&& getArguments().getBoolean(
						Constants.EXTRA_OAUTH_TOKEN_RECEIVED)) {
			getImages();
		} else
			refresh();
	}

	private void getImages() {
		List<Release> releases = mDataManager.getReleases();

		String thumb = "";

		for (Release release : releases) {
			final long releaseId = release.getId();
			if (release.getThumb().contains("image/")) {
				thumb = release.getThumb().split("image/")[1];
			} else
				continue;

			Log.d(Constants.TAG, "Attempting to get thumbnail: " + thumb);
			mAPI.getImageFromURL(thumb, new Callback<ImageResponse>() {

				@Override
				public void failure(RetrofitError error) {
					Log.d(Constants.TAG,
							"Failed to get image: " + error.getMessage());
				}

				@Override
				public void success(ImageResponse imageResponse,
						Response response) {
					Log.d(Constants.TAG,
							"Successfully got image! ImageResponse:  "
									+ imageResponse.toString());
					mDataManager.updateReleaseThumbnail(releaseId,
							imageResponse.getImage());
				}
			});
		}
		refreshAdapterAndList();
	}

	@Override
	public void onListItemClick(ListView listView, View view, int position,
			long id) {
		super.onListItemClick(listView, view, position, id);

		// Notify the active callbacks interface (the activity, if the
		// fragment is attached to one) that an item has been selected.
		TextView title = (TextView) view.findViewById(R.id.title);
		long releaseId = (Long) title.getTag();
		mCallbacks.onItemSelected(releaseId);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// Activities containing this fragment must implement its callbacks.
		if (!(activity instanceof Callbacks)) {
			throw new IllegalStateException(
					"Activity must implement fragment's callbacks.");
		}

		mCallbacks = (Callbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();

		// Reset the active callbacks interface to the dummy implementation.
		mCallbacks = sDummyCallbacks;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (mActivatedPosition != ListView.INVALID_POSITION) {
			// Serialize and persist the activated item position.
			outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
		}
	}

	/**
	 * Turns on activate-on-click mode. When this mode is on, list items will be
	 * given the 'activated' state when touched.
	 */
	public void setActivateOnItemClick(boolean activateOnItemClick) {
		// When setting CHOICE_MODE_SINGLE, ListView will automatically
		// give items the 'activated' state when touched.
		getListView().setChoiceMode(
				activateOnItemClick ? ListView.CHOICE_MODE_SINGLE
						: ListView.CHOICE_MODE_NONE);
	}

	private void setActivatedPosition(int position) {
		if (position == ListView.INVALID_POSITION) {
			getListView().setItemChecked(mActivatedPosition, false);
		} else {
			getListView().setItemChecked(position, true);
		}

		mActivatedPosition = position;
	}

	private void refresh() {
		if (NetworkUtil.isInternetAvailable(mActivity))
			updateReleases();
		else {
			if (isAdded())
				Toast.makeText(mActivity, getString(R.string.no_connection),
						Toast.LENGTH_LONG).show();

			restoreFromDB();
		}
	}

	private void restoreFromDB() {
		if (isAdded())
			Toast.makeText(mActivity, getString(R.string.showing_last_results),
					Toast.LENGTH_LONG).show();
	}

	private void updateReleases() {
		Toast.makeText(mActivity, getString(R.string.updating),
				Toast.LENGTH_LONG).show();

		mAPI.getReleasesForArtist(getString(R.string.artist_id),
				new Callback<ReleaseResponse>() {

					@Override
					public void failure(RetrofitError error) {
						Log.d(Constants.TAG, "Failed to get releases. Error: "
								+ error.getMessage());
						Toast.makeText(mActivity,
								getString(R.string.could_not_fetch),
								Toast.LENGTH_LONG).show();
					}

					@Override
					public void success(ReleaseResponse releaseResponse,
							Response response) {
						Log.d(Constants.TAG,
								"Success getting releases. ReleaseResponse: "
										+ releaseResponse.toString());
						saveReleasesInDB(releaseResponse.getReleases());
					}

					@SuppressWarnings("unchecked")
					private void saveReleasesInDB(List<Release> releases) {
						new AsyncTask<List<Release>, Void, String>() {

							@Override
							protected String doInBackground(
									List<Release>... params) {
								mDataManager.deleteReleases();
								for (Release release : params[0])
									mDataManager.saveRelease(release);
								return null;
							}

							@Override
							protected void onPostExecute(String result) {
								if (isAdded())
									Toast.makeText(mActivity,
											getString(R.string.data_saved),
											Toast.LENGTH_LONG).show();

								refreshAdapterAndList();
								if (Preferences.getStringPreference(mActivity,
										Preferences.PREF_JSONPARSER_AUTH_TOKEN,
										null) == null) {
									showAuthRequestDialog();
								}
							}
						}.execute(releases);

					}
				});
	}

	private void showAuthRequestDialog() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				mActivity);

		alertDialogBuilder
				.setTitle(getString(R.string.request_auth_dialog_title));
		alertDialogBuilder
				.setMessage(getString(R.string.request_auth_dialog_message));
		alertDialogBuilder.setCancelable(true);
		alertDialogBuilder.setPositiveButton(android.R.string.cancel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		alertDialogBuilder.setNegativeButton(android.R.string.ok,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						mCallbacks.authenticateUser();
					}
				});
		alertDialogBuilder.create().show();
	}

	private void refreshAdapterAndList() {
		mAdapter.swapCursor(mDataManager.getReleasesCursor());
		mAdapter.notifyDataSetChanged();
	}

	/**
	 * A callback interface that all activities containing this fragment must
	 * implement. This mechanism allows activities to be notified of item
	 * selections.
	 */
	public interface Callbacks {
		public void onItemSelected(long releaseId);

		public void authenticateUser();
	}

	/**
	 * A dummy implementation of the {@link Callbacks} interface that does
	 * nothing. Used only when this fragment is not attached to an activity.
	 */
	private static Callbacks sDummyCallbacks = new Callbacks() {
		@Override
		public void onItemSelected(long releaseId) {
		}

		@Override
		public void authenticateUser() {

		}
	};

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_about:
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					mActivity);

			alertDialogBuilder.setTitle(getString(R.string.about_jsonparser));
			alertDialogBuilder
					.setMessage(getString(R.string.about_jsonparser_developer)
							+ "\n" + getString(R.string.version) + "\n"
							+ getString(R.string.about_jsonparser_support));
			alertDialogBuilder.setCancelable(true);
			alertDialogBuilder.setPositiveButton(
					getString(R.string.email_support),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							Intent it = new Intent(Intent.ACTION_SEND);
							it.putExtra(
									Intent.EXTRA_EMAIL,
									new String[] { getString(R.string.support_email) });
							it.putExtra(Intent.EXTRA_SUBJECT,
									getString(R.string.app_name));
							it.putExtra(Intent.EXTRA_TEXT,
									getString(R.string.app_name) + ", "
											+ getString(R.string.version));
							it.setType("text/plain");
							startActivity(Intent.createChooser(it,
									"Choose Email Client"));
						}

					});
			alertDialogBuilder.setNegativeButton(android.R.string.ok,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.cancel();
						}
					});
			alertDialogBuilder.create().show();

			break;
		case R.id.action_refresh:
			refresh();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

}
