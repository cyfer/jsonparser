package uk.co.cyferapps.jsonparser.database;

import java.util.List;

import uk.co.cyferapps.jsonparser.discogs.api.ReleaseDetailsResponse;
import uk.co.cyferapps.jsonparser.domain.Image;
import uk.co.cyferapps.jsonparser.domain.Release;
import android.database.Cursor;

public interface DataManager {

	public List<Release> getReleases();

	public Cursor getReleasesCursor();

	public ReleaseDetailsResponse getReleaseDetails(long releaseId);

	public List<Image> getImagesForRelease(long releaseId);

	public long saveRelease(Release release);

	public long saveReleaseDetails(ReleaseDetailsResponse releaseDetails);

	public long saveImage(Image image);

	public void updateReleaseThumbnail(long releaseId, byte[] thumbInBytes);

	public void updateReleaseImage(long releaseId, String uri,
			byte[] imageBytes);

	public void deleteReleaseDetails(long releaseId);
	
	public void deleteImagesForRelease(long releaseId);

	public void deleteReleases();

	public void deleteImages();

}
