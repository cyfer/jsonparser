package uk.co.cyferapps.jsonparser.database;

import uk.co.cyferapps.jsonparser.domain.Release;
import uk.co.cyferapps.jsonparser.util.ConvertUtil;
import uk.co.cyferapps.jsonparser.R;
import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ReleaseCursorAdapter extends CursorAdapter{
	
	private final LayoutInflater mInflator;

	public ReleaseCursorAdapter(Context context, Cursor c) {
		super(context, c, true);
		mInflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		populateView(context, view, cursor);
	}
	
	private void populateView(final Context context, final View listItem, final Cursor cursor){
		final ViewHolder holder = (ViewHolder) listItem.getTag();
		if((cursor != null) && !cursor.isClosed()){
			Release release = ReleaseDao.buildRelease(cursor);
			holder.title.setText(release.getTitle());
			holder.title.setTag(release.getId());
			if(release.getThumbNailbyteArray() != null){
			Drawable thumbnail = ConvertUtil.getDrawableFromBytes(release.getThumbNailbyteArray());
			holder.title.setCompoundDrawables(thumbnail, null, null, null);
			}
			holder.year.setText(release.getYear());
		}
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		View listItem = mInflator.inflate(R.layout.release_list_item, parent, false);
		TextView title = (TextView) listItem.findViewById(R.id.title);
		TextView year = (TextView) listItem.findViewById(R.id.year);
		ViewHolder holder = new ViewHolder(title, year);
		listItem.setTag(holder);
		populateView(context, listItem, cursor);
		return listItem;
	}
	
	private static class ViewHolder {
		
		protected final TextView title;
		protected final TextView year;
		
		public ViewHolder(TextView title, TextView year){
			this.title = title;
			this.year = year;
		}
	}

}
