package uk.co.cyferapps.jsonparser.database;

import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class ReleaseDetailsTable {

	public static final String TABLE_NAME = "release_details";

	public static class ReleaseDetailsColumns implements BaseColumns {
		public static final String RELEASEID = "release_id";
		public static final String TITLE = "title";
		public static final String YEAR = "year";
		public static final String URL = "url";
		public static final String COUNTRY = "country";
		public static final String NOTES = "notes";

	}

	public static void onCreate(SQLiteDatabase db) {
		StringBuilder sb = new StringBuilder();
		sb.append("CREATE TABLE " + TABLE_NAME + "(");
		sb.append(ReleaseDetailsColumns._ID + " INTEGER PRIMARY KEY, ");
		sb.append(ReleaseDetailsColumns.RELEASEID + " TEXT NOT NULL, ");
		sb.append(ReleaseDetailsColumns.TITLE + " TEXT NOT NULL, ");
		sb.append(ReleaseDetailsColumns.YEAR + " TEXT NOT NULL, ");
		sb.append(ReleaseDetailsColumns.URL + " TEXT, ");
		sb.append(ReleaseDetailsColumns.COUNTRY + " TEXT, ");
		sb.append(ReleaseDetailsColumns.NOTES + " TEXT");
		sb.append(");");
		db.execSQL(sb.toString());
	}
	
	public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME);
		onCreate(db);
	}
}
