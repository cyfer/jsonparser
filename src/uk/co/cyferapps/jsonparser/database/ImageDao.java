package uk.co.cyferapps.jsonparser.database;

import java.util.ArrayList;
import java.util.List;

import uk.co.cyferapps.jsonparser.database.ImageTable.ImageColumns;
import uk.co.cyferapps.jsonparser.database.ReleaseTable.ReleaseColumns;
import uk.co.cyferapps.jsonparser.domain.Image;
import uk.co.cyferapps.jsonparser.util.Constants;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

public class ImageDao {
	private static String INSERT = "insert into " + ImageTable.TABLE_NAME
			+ "(" + ImageColumns.RELEASEID + ", " + ImageColumns.URL + ", "
			+ ImageColumns.TYPE + ", " + ImageColumns.IMAGEBYTES + ") values (?,?,?,?)";

	private SQLiteDatabase mDb;
	private SQLiteStatement mInsertStatement;

	public ImageDao(SQLiteDatabase db) {
		mDb = db;
		mInsertStatement = mDb.compileStatement(INSERT);
	}
	
	public void deleteImages() {
		mDb.delete(ImageTable.TABLE_NAME, null, null);
	}
	
	public void deleteImagesForRelease(long releaseId) {
		mDb.delete(ImageTable.TABLE_NAME, ImageColumns.RELEASEID + "=" + releaseId, null);
	}
	
	public Cursor getCursor() {
		return mDb.query(ImageTable.TABLE_NAME, null, null, null, null,
				null, null);
	}
	
	public List<Image> getImagesForRelease(long releaseId) {
		List<Image> images = new ArrayList<Image>();
		Cursor c = mDb.query(ImageTable.TABLE_NAME, null, ImageColumns.RELEASEID + "=?", new String[] { String.valueOf(releaseId) }, null, null, null);
		if (c.moveToFirst()) {
			do {
				Image image = new Image();
				image.setReleaseId(c.getLong(1));
				image.setUri((c.getString(2)));
				image.setType((c.getString(3)));
				image.setImageBinary(c.getBlob(4));
				images.add(image);
			} while (c.moveToNext());
		}
		if (!c.isClosed())
			c.close();
		
		return images;
	}
	
	public long saveImage(Image image) {
		mInsertStatement.clearBindings();
		mInsertStatement.bindLong(1, image.getReleaseId());
		mInsertStatement.bindString(2, image.getUri());
		mInsertStatement.bindString(3, image.getType());
		return mInsertStatement.executeInsert();
	}
	
	public void updateReleaseImage(long releaseId, String uri, byte[] imageBytes) {
		final ContentValues values = new ContentValues();
		values.put(ReleaseColumns.THUMB, imageBytes);
		mDb.update(ReleaseTable.TABLE_NAME, values, ImageColumns.RELEASEID + "="+releaseId +" AND " + ImageColumns.URL + "=" + uri , null);
	}
	
	
}
