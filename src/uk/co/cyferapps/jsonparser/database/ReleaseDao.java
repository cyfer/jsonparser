package uk.co.cyferapps.jsonparser.database;

import java.util.ArrayList;

import uk.co.cyferapps.jsonparser.database.ReleaseTable.ReleaseColumns;
import uk.co.cyferapps.jsonparser.domain.Release;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

public class ReleaseDao {
	private static String INSERT = "insert into " + ReleaseTable.TABLE_NAME
			+ "(" + ReleaseColumns.RELEASEID + ", " + ReleaseColumns.TITLE + ", "
			+ ReleaseColumns.YEAR + ", " + ReleaseColumns.THUMBURL + ", "
			+ ReleaseColumns.THUMB + ") values (?,?,?,?,?)";

	private SQLiteDatabase mDb;
	private SQLiteStatement mInsertStatement;

	public ReleaseDao(SQLiteDatabase db) {
		mDb = db;
		mInsertStatement = mDb.compileStatement(INSERT);
	}

	public long saveRelease(Release release) {
		mInsertStatement.clearBindings();
		mInsertStatement.bindLong(1, release.getId());
		mInsertStatement.bindString(2, release.getTitle());
		mInsertStatement.bindString(3, release.getYear());
		mInsertStatement.bindString(4, release.getThumb() == null ? "" : release.getThumb());
		return mInsertStatement.executeInsert();
	}

	public void deleteReleases() {
		mDb.delete(ReleaseTable.TABLE_NAME, null, null);
	}
	
	public void updateReleaseThumbnail(long releaseId, byte[] imageBytes) {
		final ContentValues values = new ContentValues();
		values.put(ReleaseColumns.THUMB, imageBytes);
		mDb.update(ReleaseTable.TABLE_NAME, values, ReleaseColumns.RELEASEID + " ="+releaseId, null);
	}

	public ArrayList<Release> getReleases() {
		ArrayList<Release> storedReleases = new ArrayList<Release>();
		Cursor c = getCursor();

		if (c.moveToFirst()) {
			do {
				Release release = new Release();
				release.setId(c.getLong(1));
				release.setTitle(c.getString(2));
				release.setYear(c.getString(3));
				release.setThumb(c.getString(4));
				release.setThumbNailbyteArray(c.getBlob(5));
				storedReleases.add(release);
			} while (c.moveToNext());
		}

		if (!c.isClosed())
			c.close();

		return storedReleases;
	}

	public Cursor getCursor() {
		return mDb.query(ReleaseTable.TABLE_NAME, null, null, null, null,
				null, null);
	}
	
	public static Release buildRelease(Cursor c) {
		Release release = null;
		if ( c != null){
			release = new Release();
			release.setId(c.getLong(1));
			release.setTitle(c.getString(2));
			release.setYear(c.getString(3));
			release.setThumb(c.getString(4));
			release.setThumbNailbyteArray(c.getBlob(5));
		}
		return release;
	}
}
