package uk.co.cyferapps.jsonparser.database;

import uk.co.cyferapps.jsonparser.database.ReleaseDetailsTable.ReleaseDetailsColumns;
import uk.co.cyferapps.jsonparser.discogs.api.ReleaseDetailsResponse;
import uk.co.cyferapps.jsonparser.domain.Image;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

public class ReleaseDetailsDao {
	private static String INSERT = "insert into " + ReleaseDetailsTable.TABLE_NAME
			+ "(" + ReleaseDetailsColumns.RELEASEID + ", " + ReleaseDetailsColumns.TITLE + ", "
			+ ReleaseDetailsColumns.YEAR + ", " + ReleaseDetailsColumns.URL + ", "
			+ ReleaseDetailsColumns.COUNTRY + ", " + ReleaseDetailsColumns.NOTES  + ") values (?,?,?,?,?,?)";

	private SQLiteDatabase mDb;
	private SQLiteStatement mInsertStatement;

	public ReleaseDetailsDao(SQLiteDatabase db) {
		mDb = db;
		mInsertStatement = mDb.compileStatement(INSERT);
	}

	public void deleteReleaseDetails(long releaseId) {
		mDb.delete(ReleaseDetailsTable.TABLE_NAME, ReleaseDetailsColumns.RELEASEID + "=" +String.valueOf(releaseId), null);
	}

	public Cursor getCursor() {
		return mDb.query(ReleaseDetailsTable.TABLE_NAME, null, null, null, null,
				null, null);
	}
	
	public ReleaseDetailsResponse getReleaseDetails(long id) {
		Cursor c = mDb.query(ReleaseDetailsTable.TABLE_NAME, null, ReleaseDetailsColumns.RELEASEID + " =?", new String[] { String.valueOf(id) }, null, null, null);
		return buildReleaseDetails(c);
	}
	
	public static ReleaseDetailsResponse buildReleaseDetails(Cursor c) {
		ReleaseDetailsResponse releaseDetails = null;
		if ( c != null && c.moveToFirst()){
			releaseDetails = new ReleaseDetailsResponse();
			releaseDetails.setId(c.getLong(1));
			releaseDetails.setTitle(c.getString(2));
			releaseDetails.setYear(c.getString(3));
			releaseDetails.setUri(c.getString(4));
			releaseDetails.setCountry(c.getString(5));
			releaseDetails.setNotes(c.getString(6));
		}
		return releaseDetails;
	}

	public long saveReleaseDetails(ReleaseDetailsResponse releaseDetails) {
		mInsertStatement.clearBindings();
		mInsertStatement.bindLong(1, releaseDetails.getId());
		mInsertStatement.bindString(2, releaseDetails.getTitle());
		mInsertStatement.bindString(3, releaseDetails.getYear());
		mInsertStatement.bindString(4, releaseDetails.getUri() == null ? "" : releaseDetails.getUri());
		mInsertStatement.bindString(5, releaseDetails.getCountry() == null ? "" : releaseDetails.getCountry());
		mInsertStatement.bindString(6, releaseDetails.getNotes() == null ? "" : releaseDetails.getNotes());
		return mInsertStatement.executeInsert();
	}
}
