package uk.co.cyferapps.jsonparser.database;

import java.util.List;

import uk.co.cyferapps.jsonparser.discogs.api.ReleaseDetailsResponse;
import uk.co.cyferapps.jsonparser.domain.Image;
import uk.co.cyferapps.jsonparser.domain.Release;
import uk.co.cyferapps.jsonparser.util.Constants;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataManagerImpl implements DataManager{
	private Context mContext;
	private SQLiteDatabase mDb;
	private ReleaseDao mReleaseDao;
	private ReleaseDetailsDao mReleaseDetailsDao;
	private ImageDao mImageDao;
	
	public DataManagerImpl(Context context){
		mContext = context;
		SQLiteOpenHelper dbHelper = new OpenHelper(mContext);
		mDb = dbHelper.getWritableDatabase();
		mReleaseDao = new ReleaseDao(mDb);
		mReleaseDetailsDao = new ReleaseDetailsDao(mDb);
		mImageDao = new ImageDao(mDb);
	}

	@Override
	public List<Release> getReleases() {
		return mReleaseDao.getReleases();
	}

	@Override
	public long saveRelease(Release release) {
		return mReleaseDao.saveRelease(release);
	}

	@Override
	public void deleteReleases() {
		mReleaseDao.deleteReleases();
	}
	

	@Override
	public void updateReleaseThumbnail(long releaseId, byte[] bitmap) {
		mReleaseDao.updateReleaseThumbnail(releaseId, bitmap);
	}
	
	@Override
	public Cursor getReleasesCursor() {
		return mReleaseDao.getCursor();
	}
	

	@Override
	public ReleaseDetailsResponse getReleaseDetails(long releaseId) {
		return mReleaseDetailsDao.getReleaseDetails(releaseId);
	}

	@Override
	public long saveReleaseDetails(ReleaseDetailsResponse releaseDetails) {
		 for (Image image : releaseDetails.getImages()){
			 image.setReleaseId(releaseDetails.getId());
		 mImageDao.saveImage(image);
		 Log.d(Constants.TAG, "Saved image: " + image.getUri());
		 }
		return mReleaseDetailsDao.saveReleaseDetails(releaseDetails);
	}

	@Override
	public void deleteReleaseDetails(long releaseId) {
		mReleaseDetailsDao.deleteReleaseDetails(releaseId);
		mImageDao.deleteImagesForRelease(releaseId);
	}
	
	@Override
	public void deleteImagesForRelease(long releaseId) {
		mImageDao.deleteImagesForRelease(releaseId);
	}
	
	@Override
	public List<Image> getImagesForRelease(long releaseId) {
		return mImageDao.getImagesForRelease(releaseId);
	}

	@Override
	public long saveImage(Image image) {
		return mImageDao.saveImage(image);
	}

	@Override
	public void updateReleaseImage(long releaseId, String uri,
			byte[] imageBytes) {
		mImageDao.updateReleaseImage(releaseId, uri, imageBytes);
		
	}

	@Override
	public void deleteImages() {
		mImageDao.deleteImages();
		
	}
	
	public class OpenHelper extends SQLiteOpenHelper {
		public static final String DB_NAME = "jsonParserDB";
		public static final int DB_VERSION = 1;

		public OpenHelper(Context context) {
			super(context, DB_NAME, null, DB_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			ReleaseTable.onCreate(db);
			ReleaseDetailsTable.onCreate(db);
			ImageTable.onCreate(db);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			ReleaseTable.onUpgrade(db, oldVersion, newVersion);
			ReleaseDetailsTable.onUpgrade(db, oldVersion, newVersion);
			ImageTable.onUpgrade(db, oldVersion, newVersion);
		}

	}









}
