package uk.co.cyferapps.jsonparser.database;

import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class ReleaseTable {
	public static final String TABLE_NAME = "releases";

	public static class ReleaseColumns implements BaseColumns {
		public static final String RELEASEID = "release_id";
		public static final String TITLE = "title";
		public static final String YEAR = "year";
		public static final String THUMBURL = "thumb_url";
		public static final String THUMB = "thumb";

	}

	public static void onCreate(SQLiteDatabase db) {
		StringBuilder sb = new StringBuilder();
		sb.append("CREATE TABLE " + TABLE_NAME + "(");
		sb.append(ReleaseColumns._ID + " INTEGER PRIMARY KEY, ");
		sb.append(ReleaseColumns.RELEASEID + " TEXT NOT NULL, ");
		sb.append(ReleaseColumns.TITLE + " TEXT NOT NULL, ");
		sb.append(ReleaseColumns.YEAR + " TEXT NOT NULL, ");
		sb.append(ReleaseColumns.THUMBURL + " TEXT, ");
		sb.append(ReleaseColumns.THUMB + " BLOB");
		sb.append(");");
		db.execSQL(sb.toString());
	}
	
	public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME);
		onCreate(db);
	}

}
