package uk.co.cyferapps.jsonparser.database;

import uk.co.cyferapps.jsonparser.database.ReleaseTable.ReleaseColumns;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class ImageTable {
	public static final String TABLE_NAME = "images";

	public static class ImageColumns implements BaseColumns {
		public static final String RELEASEID = "release_id";
		public static final String URL = "url";
		public static final String TYPE = "type";
		public static final String IMAGEBYTES = "image_bytes";
	}
	
	
	public static void onCreate(SQLiteDatabase db) {
		StringBuilder sb = new StringBuilder();
		sb.append("CREATE TABLE " + TABLE_NAME + "(");
		sb.append(ImageColumns._ID + " INTEGER PRIMARY KEY, ");
		sb.append(ImageColumns.RELEASEID + " TEXT NOT NULL, ");
		sb.append(ImageColumns.URL + " TEXT NOT NULL, ");
		sb.append(ImageColumns.TYPE + " TEXT NOT NULL, ");
		sb.append(ImageColumns.IMAGEBYTES + " BLOB");
		sb.append(");");
		db.execSQL(sb.toString());
	}
	
	public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME);
		onCreate(db);
	}
	
}
