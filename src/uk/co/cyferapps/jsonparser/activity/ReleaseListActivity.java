package uk.co.cyferapps.jsonparser.activity;

import oauth.signpost.OAuth;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthProvider;
import uk.co.cyferapps.jsonparser.R;
import uk.co.cyferapps.jsonparser.fragment.FragmentId;
import uk.co.cyferapps.jsonparser.fragment.ReleaseDetailFragment;
import uk.co.cyferapps.jsonparser.fragment.ReleaseListFragment;
import uk.co.cyferapps.jsonparser.util.Constants;
import uk.co.cyferapps.jsonparser.util.Preferences;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

/**
 * An activity representing a list of Artists. This activity has different
 * presentations for handset and tablet-size devices. On handsets, the activity
 * presents a list of items, which when touched, lead to a
 * {@link ReleaseDetailActivity} representing item details. On tablets, the
 * activity presents the list of items and item details side-by-side using two
 * vertical panes.
 * This activity also implements the required
 * {@link ReleaseListFragment.Callbacks} interface to listen for item
 * selections.
 */

public class ReleaseListActivity extends FragmentActivity implements
		ReleaseListFragment.Callbacks {
	private boolean mTwoPane;
	private OAuthProvider mProvider;
	private OAuthConsumer mConsumer;
	private FragmentManager mFragmentManager = getSupportFragmentManager();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.release_list_container);
		try {
			mConsumer = new CommonsHttpOAuthConsumer(
					getString(R.string.consumer_key),
					getString(R.string.consumer_secret));
			mProvider = new CommonsHttpOAuthProvider(
					getString(R.string.request_token_url),
					getString(R.string.access_token_url),
					getString(R.string.authorize_url));
			mProvider.setRequestHeader("User-Agent", "JSONParser");
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (findViewById(R.id.artist_detail_container) != null) {
			// The detail container view will be present only in the
			// large-screen layouts (res/values-large and
			// res/values-sw600dp). If this view is present, then the
			// activity should be in two-pane mode.
			mTwoPane = true;
			Log.d(Constants.TAG, "two-pane activity");
		}
		Bundle bundle = new Bundle();
		bundle.putBoolean(Constants.EXTRA_TWO_PANE, mTwoPane);
		onNextFragmentSelected(FragmentId.FRAGMENT_RELEASE_LIST, bundle);
	}

	private void onNextFragmentSelected(FragmentId id, Bundle bundle) {
		Fragment fragment;
		switch (id) {
		case FRAGMENT_RELEASE_LIST:
			fragment = new ReleaseListFragment();
			fragment.setArguments(bundle);
			mFragmentManager.beginTransaction()
					.replace(R.id.release_list_content_frame, fragment)
					.commit();
			break;
		}
	}

	/**
	 * Callback method from {@link ReleaseListFragment.Callbacks} indicating
	 * that the item with the given ID was selected.
	 */
	@Override
	public void onItemSelected(long releaseId) {
		if (mTwoPane) {
			// In two-pane mode, show the detail view in this activity by
			// adding or replacing the detail fragment using a
			// fragment transaction.
			Bundle arguments = new Bundle();
			arguments.putLong(Constants.RELEASE_ID, releaseId);
			ReleaseDetailFragment fragment = new ReleaseDetailFragment();
			fragment.setArguments(arguments);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.artist_detail_container, fragment).commit();

		} else {
			// In single-pane mode, simply start the detail activity
			// for the selected release ID.
			Log.d(Constants.TAG, "Starting details fragment for release: "
					+ releaseId);
			Intent detailIntent = new Intent(this, ReleaseDetailActivity.class);
			detailIntent.putExtra(Constants.RELEASE_ID, releaseId);
			startActivity(detailIntent);
		}
	}

	@Override
	public void authenticateUser() {

		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				try {
					final String url = mProvider.retrieveRequestToken(
							mConsumer, getString(R.string.callback_url));
					Intent intent = new Intent(Intent.ACTION_VIEW,
							Uri.parse(url))
							.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
					startActivity(intent);
				} catch (Exception e) {
					Log.e(Constants.TAG,
							"Error getting oAUTH retrieve request token", e);
				}
				return null;
			}
		}.execute();
	}

	private void retrieveAccessToken(Uri uri) {
		new AsyncTask<Uri, Void, String>() {

			@Override
			protected String doInBackground(Uri... params) {
				final Uri uri = params[0];
				final String verifier = uri
						.getQueryParameter(OAuth.OAUTH_VERIFIER);
				try {
					Log.d(Constants.TAG, "outh_verifier: " + verifier);
					mProvider.retrieveAccessToken(mConsumer, verifier);
					Preferences.setStringPreference(ReleaseListActivity.this,
							Preferences.PREF_JSONPARSER_AUTH_TOKEN,
							mConsumer.getToken());
					Preferences.setStringPreference(ReleaseListActivity.this,
							Preferences.PREF_JSONPARSER_AUTH_TOKEN_SECRET,
							mConsumer.getTokenSecret());
					Log.d(Constants.TAG,
							"oAUTH token obtained and saved successfully");
				} catch (Exception e) {
					Log.e(Constants.TAG,
							"Error getting oAUTH retrieve access token", e);
				}
				return null;
			}

			@Override
			protected void onPostExecute(String result) {
				if (Preferences.getStringPreference(ReleaseListActivity.this,
						Preferences.PREF_JSONPARSER_AUTH_TOKEN, null) != null) {
					Toast.makeText(ReleaseListActivity.this,
							getString(R.string.token_obtained),
							Toast.LENGTH_LONG).show();
					Bundle bundle = new Bundle();
					bundle.putBoolean(Constants.EXTRA_OAUTH_TOKEN_RECEIVED,
							true);
					onNextFragmentSelected(FragmentId.FRAGMENT_RELEASE_LIST,
							bundle);
				}
			}

		}.execute(uri);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		final Uri uri = intent.getData();
		if (uri != null
				&& uri.getScheme().equals(getString(R.string.callback_scheme))) {
			Log.d(Constants.TAG, "Authorize token callback received: " + uri);
			retrieveAccessToken(uri);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
