package uk.co.cyferapps.jsonparser.activity;

import uk.co.cyferapps.jsonparser.R;
import uk.co.cyferapps.jsonparser.fragment.FragmentId;
import uk.co.cyferapps.jsonparser.fragment.ReleaseDetailFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;

public class ReleaseDetailActivity extends FragmentActivity {

	private FragmentManager mFragmentManager = getSupportFragmentManager();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.release_details_container);

		// Show the Up button in the action bar.
		getActionBar().setDisplayHomeAsUpEnabled(true);
		onNextFragmentSelected(FragmentId.FRAGMENT_RELEASE_DETAILS, getIntent().getExtras());
	}
	
	
	private void onNextFragmentSelected(FragmentId id, Bundle bundle) {
		Fragment fragment;
		switch(id){
		case FRAGMENT_RELEASE_DETAILS:
			fragment = new ReleaseDetailFragment();
			fragment.setArguments(bundle);
			setTitle("Release Details");
			mFragmentManager.beginTransaction().replace(R.id.release_details_content_frame, fragment).commit();
			break;
		default:
			fragment = new ReleaseDetailFragment();
			fragment.setArguments(bundle);
			setTitle("Release Details");
			mFragmentManager.beginTransaction().replace(R.id.release_details_content_frame, fragment).commit();
			break;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpTo(this, new Intent(this,
					ReleaseListActivity.class));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
}
