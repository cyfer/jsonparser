package uk.co.cyferapps.jsonparser.discogs.api;

import java.util.List;

import uk.co.cyferapps.jsonparser.domain.Release;

import com.google.gson.annotations.Expose;

public class ReleaseResponse {
	
	@Expose
	private List<Release> releases;

	public List<Release> getReleases() {
		return releases;
	}

	public void setReleases(List<Release> releases) {
		this.releases = releases;
	}
	
	@Override
	public String toString(){
		return String.format("{ releases: %s", releases.toString());
	}

}
