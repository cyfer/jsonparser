package uk.co.cyferapps.jsonparser.discogs.api;
import retrofit.Callback;
import retrofit.http.*;

public interface DiscogsAPI {
	
	@GET("/artists/{artist_id}/releases")
	public void getReleasesForArtist(@Path("artist_id") String artistId, Callback<ReleaseResponse> cb);
	
	@GET("/releases/{release_id}")
	public void getReleaseDetails(@Path("release_id") String releaseId, Callback<ReleaseDetailsResponse> cb);
	
	@GET("/images/{filename}")
	public void getImageFromURL(@Path("filename") String filename, Callback<ImageResponse> cb);

}
