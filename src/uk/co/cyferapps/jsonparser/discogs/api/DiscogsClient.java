package uk.co.cyferapps.jsonparser.discogs.api;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.android.AndroidLog;
import retrofit.converter.GsonConverter;
import uk.co.cyferapps.jsonparser.util.Constants;
import uk.co.cyferapps.jsonparser.util.Preferences;
import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class DiscogsClient {
	
	public final Gson gson;
	public final DiscogsAPI api;
	
	public DiscogsClient(final String rootURL, final Context context) {
		gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(rootURL).setLog(new AndroidLog(Constants.TAG)).setLogLevel(LogLevel.BASIC).setRequestInterceptor(new RequestInterceptor() {
			@Override
			public void intercept(RequestFacade request) {
				request.addHeader("Accept", "application/json");
				request.addHeader("User-Agent", "JSONParser");
				
				if (Preferences.getStringPreference(context, Preferences.PREF_JSONPARSER_AUTH_TOKEN, null) != null ){
					request.addHeader("Authorization", Preferences.getStringPreference(context, Preferences.PREF_JSONPARSER_AUTH_TOKEN, null));
				}
				
			}
			
		}).setConverter(new GsonConverter(gson)).build();
		api = restAdapter.create(DiscogsAPI.class);
	}

}
