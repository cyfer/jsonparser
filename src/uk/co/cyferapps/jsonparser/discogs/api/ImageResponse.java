package uk.co.cyferapps.jsonparser.discogs.api;

import com.google.gson.annotations.Expose;

public class ImageResponse {
	
	@Expose
	private byte[] image;
	
	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	@Override
	public String toString(){
		return String.format("{ image: %s", image.toString());
	}

}
