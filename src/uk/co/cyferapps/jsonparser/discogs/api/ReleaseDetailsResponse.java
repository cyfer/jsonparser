package uk.co.cyferapps.jsonparser.discogs.api;

import java.util.List;

import uk.co.cyferapps.jsonparser.domain.Image;

import com.google.gson.annotations.Expose;

public class ReleaseDetailsResponse {

	@Expose
	long id;
	@Expose
	String title;
	@Expose
	String year;
	@Expose
	String uri;
	@Expose
	String country;
	@Expose
	String notes;
	@Expose
	List<Image> images;
	
	
	
	public List<Image> getImages() {
		return images;
	}
	public void setImages(List<Image> images) {
		this.images = images;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	
	@Override
	public String toString(){
		return String.format("{ id:%s, title:%s, year:%s, note:%s }", id, title, year, notes );
	}
	
	
}
