package uk.co.cyferapps.jsonparser.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Release {
	
	@Expose
	@SerializedName ("main_release")
	long id;
	@Expose
	String title;
	@Expose
	String year;
	@Expose
	String thumb;
	
	byte[] thumbNailbyteArray;
	
	public byte[] getThumbNailbyteArray() {
		return thumbNailbyteArray;
	}
	public void setThumbNailbyteArray(byte[] thumbNailbyteArray) {
		this.thumbNailbyteArray = thumbNailbyteArray;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getThumb() {
		return thumb;
	}
	public void setThumb(String thumb) {
		this.thumb = thumb;
	}
	
	@Override
	public String toString(){
		return String.format("{ id:%s, title:%s, year:%s, thumb:%s }", id, title, year, thumb );
	}

}
