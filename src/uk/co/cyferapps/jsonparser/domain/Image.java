package uk.co.cyferapps.jsonparser.domain;

import com.google.gson.annotations.Expose;

public class Image {
	
	@Expose
	String uri;
	@Expose
	String type;
	
	byte[] imageBinary;
	long releaseId;

	public long getReleaseId() {
		return releaseId;
	}

	public void setReleaseId(long releaseId) {
		this.releaseId = releaseId;
	}

	public byte[] getImageBinary() {
		return imageBinary;
	}

	public void setImageBinary(byte[] imageBinary) {
		this.imageBinary = imageBinary;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
