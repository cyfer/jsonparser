package uk.co.cyferapps.jsonparser;

import uk.co.cyferapps.jsonparser.database.DataManager;
import uk.co.cyferapps.jsonparser.database.DataManagerImpl;
import uk.co.cyferapps.jsonparser.discogs.api.DiscogsClient;
import android.app.Application;

public class JSONParserApplication extends Application {

	private DiscogsClient discogsClient;
	private DataManager mDataManager;

	@Override
	public void onCreate() {
		super.onCreate();
		discogsClient = new DiscogsClient(getString(R.string.rest_api_base_url), getApplicationContext());
	}

	public DiscogsClient getDiscogsClient() {
		return discogsClient;
	}
	
	public DataManager getDataManager(){
		if (mDataManager == null)
			mDataManager = new DataManagerImpl(this);
		
		return mDataManager;
	}
}
